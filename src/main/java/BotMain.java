import events.MessageEventListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;
import java.util.EnumSet;

public class BotMain {

    public static void main(String[] args) throws LoginException {
        final String TOKEN = "";

        JDA jda = JDABuilder.create(TOKEN, EnumSet.allOf(GatewayIntent.class)).build();
        jda.addEventListener(new MessageEventListener());


    }
}
