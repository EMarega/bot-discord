package events;

import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.managers.AudioManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageEventListener extends ListenerAdapter {
    MessageReceivedEvent evento;

    PlayerManager playerManager = PlayerManager.get();

    public void onButtonClick(ButtonClickEvent event) {
        switch (event.getButton().getId()) {
            case "replay-button":
                AudioManager audioManager = event.getGuild().getAudioManager();

                audioManager.openAudioConnection(event.getMember().getVoiceState().getChannel());

                playerManager.play(event.getGuild(), event.getMessage().getContentRaw());
                event.deferEdit().queue();
                break;
            case "pause-button":
                playerManager.pause(event.getGuild(), true);
                event.deferEdit().queue();
                break;
            case "resume-button":
                playerManager.pause(event.getGuild(), false);
                event.deferEdit().queue();
                break;
        }

    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        super.onMessageReceived(event);

        AudioManager audioManager = event.getGuild().getAudioManager();

        String message = event.getMessage().getContentRaw();

        if(event.getGuild().getTextChannelsByName("musica",true).get(0) != null) {
            playerManager.setTextChannel(event.getGuild().getTextChannelsByName("musica",true).get(0));
        }

        String[] strString = message.split(" ");
        List<String> strList = new ArrayList<String>(Arrays.asList(strString));
        this.evento = event;

        System.out.println(strList);

        if(strList.get(0).equals("!play")) {
            if(strList.size() == 2) {
                audioManager.openAudioConnection(event.getMember().getVoiceState().getChannel());
                event.getGuild().getAudioManager().openAudioConnection(event.getMember().getVoiceState().getChannel());

                Button buttonRelay = Button.primary("replay-button","Tocar novamente");
                Button buttonPause = Button.primary("pause-button","Pausar");
                Button buttonResume = Button.primary("resume-button","Despausar");

                Message messageButton = new MessageBuilder().append(strList.get(1))
                        .setActionRows(ActionRow.of(buttonRelay, buttonPause, buttonResume)).build();

                TextChannel textChannel = event.getGuild().getTextChannelsByName("musica",true).get(0);
                textChannel.sendMessage(messageButton).queue();
                playerManager.play(event.getGuild(), strList.get(1));
                event.getMessage().delete();
            } else {
                sendMessage("Tem coisa errada ai cara.", event);
            }
            deleteAfter(event);
        }

        if(strList.get(0).equals("!volume")) {
            if(strList.size() == 2) {
                int volume = Integer.parseInt(strList.get(1));
                playerManager.setVolume(event.getGuild(), volume);

            }
            deleteAfter(event);
        }

        if(message.equals("!leave")) {
            audioManager.closeAudioConnection();
            deleteAfter(event);
        }

        if(message.equals("!join")) {
            audioManager.openAudioConnection(event.getMember().getVoiceState().getChannel());
            deleteAfter(event);
        }

        if(message.equals("!pause")) {
            playerManager.pause(event.getGuild(), true);
            deleteAfter(event);
        }

        if(message.equals("!resume")) {
            playerManager.pause(event.getGuild(), false);
            deleteAfter(event);
        }

        if(message.equals("!stop")) {
            playerManager.stop(event.getGuild());
            deleteAfter(event);
        }

        if(message.equals("!clear")) {
            playerManager.stopAll(event.getGuild());
            deleteAfter(event);
        }

        if(message.equals("!info")) {
            sendMessage("Tocando agora " + playerManager.infoMusic(event.getGuild()), event);
            deleteAfter(event);
        }




    }

    public static void deleteAfter(MessageReceivedEvent event) {
        if(!event.getMessage().getAuthor().getName().equals("DjDoido")) {
            event.getMessage().delete().queueAfter(10, TimeUnit.SECONDS);
        }
    }

    public void sendMessage(String message, MessageReceivedEvent event) {
        event.getMessage().reply(message).queue();
//        System.out.println(event.getMessage().getChannel());
//        event.getGuild().getTextChannelsByName("aaa", true).get(0);
    }


}
