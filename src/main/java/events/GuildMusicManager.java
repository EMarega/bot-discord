package events;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import net.dv8tion.jda.api.entities.TextChannel;

public class GuildMusicManager {

    private TextChannel textChannel;
    private AudioForwarder audioForwarder;
    private AudioEvent audioEvent;

    public GuildMusicManager(AudioPlayerManager manager, TextChannel textChannel){
        AudioPlayer player = manager.createPlayer();
        audioEvent = new AudioEvent(player);
        player.addListener(audioEvent);
        audioForwarder = new AudioForwarder(player);
        this.textChannel = textChannel;
    }

    public AudioForwarder getAudioForwarder() {
        return audioForwarder;
    }

    public AudioEvent getAudioEvent() {
        return audioEvent;
    }

}
